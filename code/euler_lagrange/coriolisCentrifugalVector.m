function c = coriolisCentrifugalVector(M, q, dq)
% c = coriolisCentrifugalVector(M, q, dq) takes as inputs:
%   -M: The inertia matrix of an open-chained manipulator robot
%   -q: The n-vector containing the symbolic expressions of the robot's
%       joints' position variables
%   -dq: The n-vector containing the symbolic expressions of the robot's
%        joints' velocity variables
% and outputs:
%   -c: The n-vector containing the coriolis and centrifugal coefficients

    % Gather the number of dofs and initialize the n_dof-vector c
    n_dof = length(q);
    c = sym(zeros(n_dof, 1));
    
    for i=1:n_dof
        % C_k = 0.5*(dM_k/dq + transpose(dM_k/dq) - dM/dq_k)
        dM_k = jacobian(M(:, i), q);
        tmp = simplify(0.5*(dM_k + transpose(dM_k) - diff(M, q(i))));
        
        % c(i) = transpose(d(q))*C_k*d(q)
        c(i) = simplify(transpose(dq)*tmp*dq);
    end
end