function [U, Us] = potentialEnergyLinks(DHParams, com_pos, q, m, g_trs)
% [U, Us] = potentialEnergyLinks(DHParams, com_pos, q, vertical_axis) takes as inputs:
%   -DHParams: a n-vector of vectors composed like this: [alpha a d theta]
%   -com_pos: a matrix of dimension nx3, where every i-th row is the 
%             transpose of the position of the com of link i w.r.t. frame i
%   -q: The n-vector containing the symbolic expressions of the robot's
%       joints' position variables
%   -m: The n-vector containing the symbolic or numeric expressions of the
%       robot's links masses.
%   -g_trs: The 3 row vector containing the g_0 acceleration only in the
%           vertical axis according to the base frame of the robot encoded
%           by DHParams.
% and outputs:
%   -U: The symbolic expression of the total potential energy of the
%       (open chained) manipulator encoded by DHParams.
%   -Us: A n-vector containing all the symbolic expressions of the partial 
%        potential energies of the links of the (open chained) manipulator 
%        encoded by DHParams.

    n_dof = length(q);
    
    % Computing all the denavit-hartenberg matrices A_i
    [~, A] = dhMatrix(DHParams);
    
    % initialize useful variables
    Us = sym(zeros(n_dof, 1));
    A_cumulative = eye(4);
    
    for i=1:n_dof
        A_cumulative = A_cumulative * A{i};
        
        pc_i = transpose(com_pos(i, :));                                            % distance from Oi to CoMi as seen from frame i
        r_0_ci = simplify(A_cumulative(1:3, 4) + A_cumulative(1:3, 1:3)*pc_i);      % distance from Oi to CoMi as seen from frame 0
                                                                                    % the latter is computed as the position of Oi
                                                                                    % + the rotated(according to frame 0) pc_i
        Us(i) = simplify(-m(i)*g_trs*r_0_ci);                                       % potential energy Ui
    end
    
    % Computing the total potential energy of the manipulator
    U = simplify(sum(Us));
    
end 