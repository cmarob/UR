function M = inertiaMatrixLinks(T, dq)
% M = inertiaMatrixLinks(T, dq) takes as inputs:
%   -Tr: The total kinetic energy of a (open-chained) manipulators' robot's
%        motors
%   -dq: The n-vector containing the symbolic expressions of the robot's
%        joints' velocity variables
% and outputs:
%   -M: The symbolic expression of the inertia matrix
    
    n_dof = length(dq);
    M = sym(zeros(n_dof));
    
    for i = 1:n_dof
        % differentiating w.r.t. dq_i and dq_j to obtain the element m_ij
        M(i,i) = diff(T, dq(i), 2);
        temp = diff(T, dq(i));
        for j = (i+1):n_dof
            coeff = diff(temp, dq(j));
            M(i, j) = coeff;
            M(j, i) = coeff;
        end
    end
end
