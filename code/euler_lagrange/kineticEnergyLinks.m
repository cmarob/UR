function [T, Ts] = kineticEnergyLinks(DHParams, com_pos, q, dq, m, I)
% [T, Ts] = kineticEnergyLinks(DHParams, com_pos, q, dq, m, I)
% takes as inputs:
%   -DHParams: a n-vector of vectors composed like this: [alpha a d theta]
%   -com_pos: a matrix of dimension nx3, where every i-th row is the 
%             transpose of the position of the com of link i w.r.t. frame i
%   -q: The n-vector containing the symbolic expressions of the robot's
%       joints' position variables
%   -dq: The n-vector containing the symbolic expressions of the robot's
%        joints' velocity variables
%   -m: The n-vector containing the symbolic or numeric expressions of the
%       robot's links masses.
%   -I: The 3xn-matrix containing the symbolic or numeric expressions of
%       the robot's links' inertia components. The i-th column contains the
%       3 diagonal inertias of link i.
% and outputs:
%   -T: The symbolic expression of the total kinetic energy of the
%       (open chained) manipulator encoded by DHParams.
%   -Ts: A n-vector containing all the symbolic expressions of the partial 
%        kinetic energies of the (open chained) manipulator encoded by
%        DHParams.

    n_dof = length(q);
    
    % Computing all the denavit-hartenberg matrices A_i
    [~, A] = dhMatrix(DHParams);
                 
    % Building a list of n_dof sigmas
    % The sigma i is 0 if the joint_i is revolute
    % and it is 1 if the joint_i is prismatic
    sigmas = zeros(1, n_dof);
    for index = 1:n_dof
        el = DHParams(index, end);
        if ~isa(el, 'sym') || ~ismember(el, q)
            sigmas(index) = 1;
        end
    end

    % initial angular/translational velocities
    v = zeros(3, 1);
    w = zeros(3, 1);
    
    z_i = [0; 0; 1]; % the z_i axis vector expressed according to frame i
    
    Ts = sym(zeros(n_dof,1)); % vector of partial kinetic energies

    for i = 1:n_dof
        R_trs = transpose(A{i}(1:3, 1:3));
        r = simplify(R_trs*A{i}(1:3, 4));                                                       % The distance from O_i-1 to O_i as seen in frame i
        w = simplify(R_trs*(w + (1-sigmas(i))*dq(i)*z_i));                                      % w_i as seen from frame i
        v = simplify(R_trs*(v + sigmas(i)*dq(i)*z_i) + cross(w, r));                            % v_i as seen from frame i
        v_com = simplify(v + cross(w, transpose(com_pos(i, :))));                               % velocity of com_i as seen in frame i
        Ts(i) = simplify(0.5*(transpose(v_com)*m(i)*v_com + transpose(w)*diag(I(:, i))*w));     % update the i-th kinetic energy    
    end
    
    T = simplify(sum(Ts)); % basically T += Ti for all i
end

