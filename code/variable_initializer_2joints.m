clear all; clc;

%%  initialize constant parameters
m1=20; m2=10;   % kg links masses
r1=0.1; r2=0.1; % m links section (full cylindrical links)
l1=0.5; l2=0.5; % m links lenghts

r_rotor1=0.045; r_rotor2=0.03;   % rotor's section(full cylindrical) [m]
l_rotor1=0.085; l_rotor2=0.075;     % rotor's lenghts [m]
m_rotor1=5; m_rotor2=3;         % rotor's masses [kg]
Bm1=m_rotor1*(r_rotor1^2)/2;  % rotor1 inertia
Bm2=m_rotor2*(r_rotor2^2)/2;  % rotor2 inertia

g0=-9.81;       % gravitational constant
K1=1000;        % spring 1 stiffness
K2=1000;        % spring 2 stiffness
Kp1=180;        % proportional gain of motor 1
Kp2=180;        % proportional gain of motor 2
Kd1=80;         % derivative gain of motor 1
Kd2=80;         % derivative gain of motor 2
Kv=[Kp1 Kp2 Kd1 Kd2];        % gains' vector
I1=(m1/12)*(3*(r1)^4+l1^2);  % inertia of first link
I2=(m2/12)*(3*(r2)^4+l2^2);  % inertia of second link

%%  intialize manipulator's variables
q10=0; q20=0; q1_dot0=0; q2_dot0=0;                         % initial link state variables
theta10=q10+.1275; theta20=q20+.0245; theta1_dot0=0; theta2_dot0=0; % initial motor state variables

initCondt=[q10 q20 theta10 theta20];                        % vectors for simulink
initCondt_dot=[q1_dot0 q2_dot0 theta1_dot0 theta2_dot0];

%%  initialize dynamic model's components
a1=I1+m1*(l1/2)^2+m2*l1^2;
a2=m2*l1*(l2/2);
a3=I2+m2*(l2/2)^2;
a4=(m1*l1/2+m2*l1);
a5=m2*l2/2;

param=[a1 a2 a3 a4 a5 Bm1 Bm2 K1 K2 l1 m_rotor2];

%% noisy params
noise_value = 0.1;
n = length(param);
noises = 1 - noise_value + (noise_value+noise_value)*rand(n,1);
noisy_param = param.*noises;

%%  desired position
q_d=[pi/3; pi/4];
a4=noisy_param(4);
a5=noisy_param(5);
K1=noisy_param(8);
K2=noisy_param(9);

g_d = g0*[-a4*cos(q_d(1))-a5*cos(q_d(1)+q_d(2)); -a5*cos(q_d(1)+q_d(2))];
theta_d=q_d+[K1 0;0 K2]\g_d;

dsrdPos=[q_d;theta_d];