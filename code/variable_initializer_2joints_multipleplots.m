clear all; clc;

%%  initialize constant parameters
m1=20; m2=10;   % kg links masses
r1=0.1; r2=0.1; % m links section (full cylindrical links)
l1=0.5; l2=0.5; % m links lenghts

r_rotor1=0.045; r_rotor2=0.03;   % rotor's section(full cylindrical) [m]
l_rotor1=0.085; l_rotor2=0.075;     % rotor's lenghts [m]
m_rotor1=5; m_rotor2=3;         % rotor's masses [kg]
Bm1=m_rotor1*(r_rotor1^2)/2;  % rotor1 inertia
Bm2=m_rotor2*(r_rotor2^2)/2;  % rotor2 inertia

g0=-9.81;       % gravitational constant
K1=1000;        % spring 1 stiffness
K2=1000;        % spring 2 stiffness
Kp1=180;        % proportional gain of motor 1
Kp2=180;        % proportional gain of motor 2
Kd1=80;         % derivative gain of motor 1
Kd2=80;         % derivative gain of motor 2
Kv=[Kp1 Kp2 Kd1 Kd2];        % gains' vector
I1=(m1/12)*(3*(r1)^4+l1^2);  % inertia of first link
I2=(m2/12)*(3*(r2)^4+l2^2);  % inertia of second link

%%  intialize manipulator's variables
q10=0; q20=0; q1_dot0=0; q2_dot0=0;                         % initial link state variables
theta10=q10+.1275; theta20=q20+.0245; theta1_dot0=0; theta2_dot0=0; % initial motor state variables

initCondt=[q10 q20 theta10 theta20];                        % vectors for simulink
initCondt_dot=[q1_dot0 q2_dot0 theta1_dot0 theta2_dot0];

%%  initialize dynamic model's components
a1=I1+m1*(l1/2)^2+m2*l1^2;
a2=m2*l1*(l2/2);
a3=I2+m2*(l2/2)^2;
a4=(m1*l1/2+m2*l1);
a5=m2*l2/2;

param=[a1 a2 a3 a4 a5 Bm1 Bm2 K1 K2 l1 m_rotor2];

%% noisy params
n = length(param);
rand_vec = rand(n,1);
noise_values = linspace(0, 0.6, 4).';

noise_value1 = noise_values(1);
noise_value2 = noise_values(2);
noise_value3 = noise_values(3);
noise_value4 = noise_values(4);

noises1 = 1 - noise_value1 + 2*noise_value1*rand_vec;
noises2 = 1 - noise_value2 + 2*noise_value2*rand_vec;
noises3 = 1 - noise_value3 + 2*noise_value3*rand_vec;
noises4 = 1 - noise_value4 + 2*noise_value4*rand_vec;

noisy_param1 = param.*noises1.';
noisy_param2 = param.*noises2.';
noisy_param3 = param.*noises3.';
noisy_param4 = param.*noises4.';

%%  desired position
q_d = [pi/2; pi/4];

%% constant gravity compensation
a41=noisy_param1(4);
a51=noisy_param1(5);
a42=noisy_param2(4);
a52=noisy_param2(5);
a43=noisy_param3(4);
a53=noisy_param3(5);
a44=noisy_param4(4);
a54=noisy_param4(5);

K11=noisy_param1(8);
K21=noisy_param1(9);
K12=noisy_param2(8);
K22=noisy_param2(9);
K13=noisy_param3(8);
K23=noisy_param3(9);
K14=noisy_param4(8);
K24=noisy_param4(9);

g_d1 = g0*[-a41*cos(q_d(1))-a51*cos(q_d(1)+q_d(2)); -a51*cos(q_d(1)+q_d(2))];
g_d2 = g0*[-a42*cos(q_d(1))-a52*cos(q_d(1)+q_d(2)); -a52*cos(q_d(1)+q_d(2))];
g_d3 = g0*[-a43*cos(q_d(1))-a53*cos(q_d(1)+q_d(2)); -a53*cos(q_d(1)+q_d(2))];
g_d4 = g0*[-a44*cos(q_d(1))-a54*cos(q_d(1)+q_d(2)); -a54*cos(q_d(1)+q_d(2))];

theta_d1=q_d+[K11 0;0 K21]\g_d1;
theta_d2=q_d+[K12 0;0 K22]\g_d2;
theta_d3=q_d+[K13 0;0 K23]\g_d3;
theta_d4=q_d+[K14 0;0 K24]\g_d4;

dsrdPos1=[q_d;theta_d1];
dsrdPos2=[q_d;theta_d2];
dsrdPos3=[q_d;theta_d3];
dsrdPos4=[q_d;theta_d4];