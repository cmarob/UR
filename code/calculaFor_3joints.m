clc; clear all;
%% Symbols
syms q1 q2 q3 dq_1 dq_2 dq_3 theta1 theta2 theta3 theta1_dot theta2_dot theta3_dot % links and rotors variables
syms K1 K2 K3 % elastic coefficients
syms m1 m2 m3 l1 l2 l3 r1 r2 r3 % masses, lengths and radius
syms d1 d2 d3 % coefficients of links center of masses
syms mr1 mr2 mr3 lr1 lr2 lr3 r_r1 r_r2 r_r3 % masses, lengths and radius and
syms I1x I1y I1z I2x I2y I2z I3x I3y I3z Ir1x Ir1y Ir1z Ir2x Ir2y Ir2z Ir3x Ir3y Ir3z
syms g0%=9.81; % gravity constant

%% State and environment vectors initialization
g = [0; 0; -g0];

q = [q1; q2; q3]; q_dot=[dq_1; dq_2; dq_3];
theta = [theta1; theta2; theta3]; theta_dot=[theta1_dot; theta2_dot; theta3_dot];

%% Inertia matrices of both rotors and links
I1 = diag([I1x I1y I1z]);
I2 = diag([I2x I2y I2z]);
I3 = diag([I3x I3y I3z]);

Ir1 = diag([Ir1x Ir1y Ir1z]);
Ir2 = diag([Ir2x Ir2y Ir2z]);
Ir3 = diag([Ir3x Ir3y Ir3z]);

%% DH Matrices
% Homogeneous matrices
A01 = [cos(q1) 0 sin(q1) 0; sin(q1) 0 -cos(q1) 0; 0 1 0 l1; 0 0 0 1];
A12 = [cos(q2) -sin(q2) 0 l2*cos(q2); sin(q2) cos(q2) 0 l2*sin(q2); 0 0 1 0; 0 0 0 1];
A23 = [cos(q3) -sin(q3) 0 l3*cos(q3); sin(q3) cos(q3) 0 l3*sin(q3); 0 0 1 0; 0 0 0 1];
A = simplify(A01*A12*A23);

% Rotation matrix inside DH Matrices
R01 = A01(1:3, 1:3);
R12 = A12(1:3, 1:3);
R23 = A23(1:3, 1:3);

r = A(1:3, 4); % direct kinematics of the E-E
J = jacobian(r, q);

%% position vectors for link and center of masses
r1=[0;l1;0];
r2=[l2;0;0];
r3=[l3;0;0];
r1c=[0;d1-l1;0];  % CoMs are positioned along link's vertical axis
r2c=[d2-l2;0;0];
r3c=[d3-l3;0;0];

%% angular and linear velocities + kinetic Energy of links
omega0=[0;0;0];
velocity0=[0;0;0];

omega1=transpose(R01)*(omega0+dq_1*[0;0;1]);
velocity1=transpose(R01)*velocity0+cross(omega1,r1);
cmVel1=velocity1+cross(omega1,r1c);
T1=.5*(m1*transpose(cmVel1)*cmVel1+transpose(omega1)*I1*omega1);

omega2=transpose(R12)*(omega1+dq_2*[0;0;1]);
velocity2=transpose(R12)*velocity1+cross(omega2,r2);
cmVel2=velocity2+cross(omega2,r2c);
T2=.5*(m2*transpose(cmVel2)*cmVel2+transpose(omega2)*I2*omega2);

omega3=transpose(R23)*(omega2+dq_3*[0;0;1]);
velocity3=transpose(R23)*velocity2+cross(omega3,r3);
cmVel3=velocity3+cross(omega3,r3c);
T3=.5*(m3*transpose(cmVel3)*cmVel3+transpose(omega3)*I3*omega3);

T=simplify(T1+T2+T3);

%% links Inertia matrix + Coriolis and centrifugal forces
Ml(1,1)=diff(T,dq_1,2);
Ml(2,2)=diff(T,dq_2,2);
Ml(3,3)=diff(T,dq_3,2);
TempMl12=diff(T,dq_1);
Ml(1,2)=diff(TempMl12,dq_2);
Ml(2,1)=Ml(1,2);
TempMl13=diff(T,dq_1);
Ml(1,3)=diff(TempMl13,dq_3);
Ml(3,1)=Ml(1,3);
TempMl23=diff(T,dq_2);
Ml(2,3)=diff(TempMl23,dq_3);
Ml(3,2)=Ml(2,3);
Ml=simplify(Ml)

M1=Ml(:,1);
C1=(1/2)*(jacobian(M1,q)+transpose(jacobian(M1,q))-diff(Ml,q1));
M2=Ml(:,2);
C2=(1/2)*(jacobian(M2,q)+transpose(jacobian(M2,q))-diff(Ml,q2));
M3=Ml(:,3);
C3=(1/2)*(jacobian(M3,q)+transpose(jacobian(M3,q))-diff(Ml,q3));
c1=simplify(collect(transpose(q_dot)*C1*q_dot))
c2=simplify(collect(transpose(q_dot)*C2*q_dot))
c3=simplify(collect(transpose(q_dot)*C3*q_dot))
% c=simplify([c1;c2;c3])

%% links' potential Energy + elastic coupling
A = A01*A12;
pc2 = A(1:3, 4) + A(1:3,1:3)*r2c;
U2 = -m2*transpose(g)*pc2;
A = A*A23;
pc3 = A(1:3, 4) + A(1:3,1:3)*r3c;
U3 = -m3*transpose(g)*pc3;
U = simplify(U2+U3);
G = transpose(jacobian(U, q));
K=diag([K1 K2 K3])*(q-theta);

%% angular and linear velocities + kinetic Energy of rotors
% first motor has null linear velocity since it is located at the base
% frame, similarly second motor as null linear velocity if it is located on
% the top of first link. Third motor has angular and linear velocities.
omega_r1=omega0+theta1_dot*[0;0;1];
velocity_r1=velocity0+cross(omega0,[0;0;0]); % vector of zeros are used in the cross product imaging the origin of the rotor's RF coinciding with the origin of the previous RF
cmVel_r1=velocity_r1+cross(omega0,[0;0;0]);
Tr1=.5*(mr1*transpose(cmVel_r1)*cmVel_r1+transpose(omega_r1)*Ir1*omega_r1);

omega_r2=omega1+theta2_dot*[0;0;1];
velocity_r2=velocity1+cross(omega1,[0;0;0]);
cmVel_r2=velocity_r2+cross(omega1,[0;0;0]);
Tr2=.5*(mr2*transpose(cmVel_r2)*cmVel_r2+transpose(omega_r2)*Ir2*omega_r2);

omega_r3=omega2+theta3_dot*[0;0;1];
velocity_r3=velocity2+cross(omega2,[0;0;0]);
cmVel_r3=velocity_r3+cross(omega2,[0;0;0]);
Tr3=.5*(mr3*transpose(cmVel_r3)*cmVel_r3+transpose(omega_r3)*Ir3*omega_r3);

Tr=simplify(Tr1+Tr2+Tr3);

%% rotors Inertia matrices
Mr(1,1)=diff(Tr,dq_1,2);
Mr(2,2)=diff(Tr,dq_2,2);
Mr(3,3)=diff(Tr,dq_3,2);
TempMr12=diff(Tr,dq_1);
Mr(1,2)=diff(TempMr12,dq_2);
Mr(2,1)=Mr(1,2);
TempMr13=diff(Tr,dq_1);
Mr(1,3)=diff(TempMr13,dq_3);
Mr(3,1)=Mr(1,3);
TempMr23=diff(Tr,dq_2);
Mr(2,3)=diff(TempMr23,dq_3);
Mr(3,2)=Mr(2,3);
Mr=simplify(Mr) % is Mr+S(B\S.')

B(1,1)=diff(Tr,theta1_dot,2);
B(2,2)=diff(Tr,theta2_dot,2);
B(3,3)=diff(Tr,theta3_dot,2);
TempB12=diff(Tr,theta1_dot);
B(1,2)=diff(TempB12,theta2_dot);
B(2,1)=B(1,2);
TempB13=diff(Tr,theta1_dot);
B(1,3)=diff(TempB13,theta3_dot);
B(3,1)=B(1,3);
TempB23=diff(Tr,theta2_dot);
B(2,3)=diff(TempB23,theta3_dot);
B(3,2)=B(2,3);
B=simplify(B)

TempS12=diff(Tr,theta2_dot);
S(1,2)=diff(TempS12,dq_1);
TempS23=diff(Tr,theta3_dot);
S(2,3)=diff(TempS23,dq_2);
TempS13=diff(Tr,theta3_dot);
S(1,3)=diff(TempS13,dq_1);
S(3,3)=0;
S=simplify(S)