clear all; clc;

syms q_1 q_2 q_3 dq_1 dq_2 dq_3
syms I_1x I_1y I_1z I_2x I_2y I_2z I_3x I_3y I_3z
syms Ir_1x Ir_1y Ir_1z Ir_2x Ir_2y Ir_2z Ir_3x Ir_3y Ir_3z
syms d_1 d_2 d_3 l_1 l_2 l_3 m_1 m_2 m_3 m_r1 m_r2 m_r3
syms g0

a1 = I_1y;
a2 = d_3^2*m_3 + I_3y;
a3 = d_3*l_2*m_3;
a4 = l_2^2*m_3 + I_2y + d_2^2*m_2;
a5 = I_3x;
a6 = I_2x;
a7 = I_2z + I_3z + d_2^2*m_2 + d_3^2*m_3 + l_2^2*m_3;
a8 = I_3z + d_3^2*m_3;
a9 = Ir_2y;
a10 = Ir_3x;
a11 = Ir_3y + l_2^2*m_r3;
a12 = Ir_3z + l_2^2*m_r3;
a13 = I_2y - I_2x + d_2^2*m_2 + l_2^2*m_3;
a14 = I_3y - I_3x + d_3^2*m_3;
a15 = d_2*m_2 + l_2*m_3;
a16 = d_3*m_3;

M = sym(zeros(3));
M(1,1) = a1 + a2*cos(q_2+q_3)^2 + a4*cos(q_2)^2 + a3*cos(q_2+q_3)*cos(q_2) + a5*sin(q_2+q_3)^2 + a6*sin(q_2)^2;
M(2,2) = a7 + 2*a3*cos(q_3);
M(2,3) = a8 + a3*cos(q_3);
M(3,2) = M(2,3);
M(3,3) = a8;
M = simplify(M);

Mr = sym(zeros(3));
Mr(1,1) = a9 + a10*sin(q_2)^2 + a11*cos(q_2)^2;
Mr(2,2) = a12;
Mr = simplify(Mr);

c1 = -dq_1*(a13*dq_2*sin(2*q_2) + a14*sin(2*q_2+2*q_3)*(dq_2 + dq_3) + a3*sin(2*q_2 + q_3)*(2*dq_2 + dq_3) + a3*sin(q_3)*dq_3)
c2 = dq_1^2*(1/2*a13*sin(2*q_2) + 1/2*a14*sin(2*q_2 + 2*q_3) + a3*sin(2*q_2+q_3)) - a3*dq_3*sin(q_3)*(2*dq_2 + dq_3)
c3 = dq_1^2*sin(q_2+q_3)*(a14*cos(q_2+q_3) + a3*cos(q_2)) + a3*dq_2^2*sin(q_3)

g2 = g0*(a15*cos(q_2) + a16*cos(q_2+q_3))
g3 = a16*g0*cos(q_2+q_3)