clear all; clc;

%%  initialize constant parameters
m_1=20; m_2=10; m_3=10;            % kg links masses
r_1=0.1; r_2=0.1; r_3=0.1;         % m links section (full cylindrical links)
l_1=0.5; l_2=0.5; l_3=0.5;         % m links lenghts
g0=9.81;                        % gravitational constant
K1=14210;                       % spring 1 stiffness
K2=29800;                       % spring 2 stiffness
K3=14210;                       % spring 3 stiffness
Kp1=95;                         % proportional gain of motor 1
Kp2=95;                         % proportional gain of motor 2
Kp3=95;                         % proportional gain of motor 3
Kd1=50;                         % derivative gain of motor 1
Kd2=50;                         % derivative gain of motor 2
Kd3=50;                         % derivative gain of motor 3
d_1 = l_1/2;            % distance CoM1 origin of frame 1
d_2 = l_2/2;                      % distance CoM2 origin of frame 2
d_3 = l_3/2;            % distance CoM3 origin of frame 3
m_r1 = 5; m_r2 = 2; m_r3 = 2;
lr1 = 0.1; lr2 = 0.05; lr3 = 0.05; 
r_r1 = 0.05; r_r2 = 0.02; r_r3 = 0.02;

I1 = m_1*diag([(r_1^2)/2 (3*r_1^4+l_1^2)/12 (3*r_1^4+l_1^2)/12]);
I2 = m_2*diag([(r_2^2)/2 (3*r_2^4+l_2^2)/12 (3*r_2^4+l_2^2)/12]);
I3 = m_3*diag([(r_3^2)/2 (3*r_3^4+l_3^2)/12 (3*r_3^4+l_3^2)/12]);

Ir1 = m_r1*diag([(r_r1^2)/2 (3*r_r1^4+lr1^2)/12 (3*r_r1^4+lr1^2)/12]);
Ir2 = m_r2*diag([(3*r_r2^4+lr2^2)/12 (3*r_r2^4+lr2^2)/12 (r_r2^2)/2]);
Ir3 = m_r3*diag([(3*r_r3^4+lr3^2)/12 (3*r_r3^4+lr3^2)/12 (r_r3^2)/2]);

a1 = I1(5);
a2 = d_3^2*m_3 + I3(5);
a3 = d_3*l_2*m_3;
a4 = l_2^2*m_3 + I2(5) + d_2^2*m_2;
a5 = I3(1);
a6 = I2(1);
a7 = I2(9) + I3(9) + d_2^2*m_2 + d_3^2*m_3 + l_2^2*m_3;
a8 = I3(9) + d_3^2*m_3;
a9 = Ir2(5);
a10 = Ir3(1);
a11 = Ir3(2) + l_2^2*m_r3;
a12 = Ir3(9) + l_2^2*m_r3;
a13 = I2(5) - I2(1) + d_2^2*m_2 + l_2^2*m_3;
a14 = I3(5) - I3(1) + d_3^2*m_3;
a15 = d_2*m_2 + l_2*m_3;
a16 = d_3*m_3;
a17 = Ir1(9);
a18 = Ir2(9);
a19 = Ir3(9);

param=[a1 a2 a3 a4 a5 a6 a7 a8 a9 a10 a11 a12 a13 a14 a15 a16 a17 a18 a19 g0 K1 K2 K3];
gains=[Kp1 Kp2 Kp3 Kd1 Kd2 Kd3];

%%  intialize manipulator's variables
q10=0; q20=pi/2; q30=0; q1_dot0=0; q2_dot0=0; q3_dot0=0;                                    % initial link state variables
theta10=0; theta20=pi/2; theta30=0; theta1_dot0=0; theta2_dot0=0; theta3_dot0=0;            % initial motor state variables

initCondt=[q10 q20 q30 theta10 theta20 theta30];                                            % vectors for simulink
initCondt_dot=[q1_dot0 q2_dot0 q3_dot0 theta1_dot0 theta2_dot0 theta3_dot0];

%%  desired position
q_d=[pi/4;pi/4;pi/4];
g_d = g0*[0;a16*cos(q_d(2)+q_d(3))+a15*cos(q_d(2));a16*cos(q_d(2)+q_d(3))];
theta_d=q_d+[K1 0 0;0 K2 0; 0 0 K3]\g_d;

dsrdPos=[q_d;theta_d];