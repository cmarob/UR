In this folder are contained all the files elaborated in order to replicate the results for the assigned work.

In the folder named 'simulink_models' are located: the model of the 2R vertical planar manipulator presented in the paper 'PD control with on-line gravity compensation for robots with elastic
joints: Theory and experiments' and the model of the 3R spatial manipuilator presented in the paper 'A simple PD controller for robots with elastic joints'.

Before execute one of the two models it is necessary to run the appropriate initializer file. These are located in the main folder and are named respectively 'variable_initializer_2joints.m' and 'variable_initializer_3joints.m'.

Edoardo Piroli
Fabio Oddi
Luca Faraoni