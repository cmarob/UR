function u = NE(q,q_dot,q_2dot)
    omega=evalin('base','omegaBase');
    omega_dot=evalin('base','omega_dotBase');
    acc=evalin('base','accBase');
    force=evalin('base','forceEE');
    torque=evalin('base','torqueEE');
    inertia=evalin('base','inertia');
    sigma=evalin('base','sigma');
    r_cm=evalin('base','r_cm');
    r_e=evalin('base','r_e');
    r_i=evalin('base','r_i');
    m=evalin('base','m');
    l_m=evalin('base','l_m');
    r_e_m=evalin('base','r_e_m');
    r_i_m=evalin('base','r_i_m');
    m_m=evalin('base','m_m');
    kr=evalin('base','kr');
    F_visc=evalin('base','F_visc');
    F_stict=evalin('base','F_stict');
    inertia_m=evalin('base','inertia_m');
    DH=evalin('base','DH');
    R_m=evalin('base','R_m');
    A=cell(1,length(sigma));
    r=cell(1,length(sigma));
    tmp=cell(1,length(sigma));
    out=zeros(length(sigma),1);
    for i=1:length(sigma)
        for j=1:4
            if DH{i}(j)==96069
                tmp{i}(j)=q(i);
            elseif DH{i}(j)-96069>0
                tmp{i}(j)=q(i)+DH{i}(j)-96069;
            else
                tmp{i}(j)=DH{i}(j);
            end
        end
        A{i}= [cos(tmp{i}(4)) -cos(tmp{i}(1))*sin(tmp{i}(4)) sin(tmp{i}(1))*sin(tmp{i}(4)) tmp{i}(2)*cos(tmp{i}(4));...
               sin(tmp{i}(4)) cos(tmp{i}(1))*cos(tmp{i}(4)) -sin(tmp{i}(1))*cos(tmp{i}(4)) tmp{i}(2)*sin(tmp{i}(4));...
                     0              sin(tmp{i}(1))               cos(tmp{i}(1))                  tmp{i}(3)      ;...
                     0                  0                             0                              1         ];
        r{i}=A{i}(1:3,1:3).'*A{i}(1:3,4); %compute position vector
        if norm(inertia{i})==0 % inertia of augmented link
            if A{i}(3,4)~=0 && A{i}(2,4)==0 && A{i}(1,4)==0
                inertia{i}(1,1)=.0833*(m(i)+m_m(i+1))*(3*(r_e(i)^2+r_i(i)^2)^2+tmp{i}(2)^2);
                inertia{i}(2,2)=inertia{i}(1,1);
                inertia{i}(3,3)=.5*(m(i)+m_m(i+1))*(r_e(i)^2+r_i(i)^2);
            else
                inertia{i}(1,1)=.5*(m(i)+m_m(i+1))*(r_e(i)^2+r_i(i)^2);
                inertia{i}(2,2)=.0833*(m(i)+m_m(i+1))*(3*(r_e(i)^2+r_i(i)^2)^2+tmp{i}(2)^2);
                inertia{i}(3,3)=inertia{i}(2,2);
            end
            inertia{i}=inertia{i}+(m(i)+m_m(i+1))*(r{i}.'*r{i}*eye(3)-r{i}*r{i}.');
        end
        if inertia_m(i)==0
            inertia_m(i)=.0833*m_m(i)*(3*(r_e_m(i)^2+r_i_m(i)^2)^2+l_m(i)^2);
        end
    end
%% here go forward and backward functions
    for i=1:length(sigma)
       [omega,omega_dot,acc,acc_cm,omega_dot_m]=forward(omega,omega_dot,acc,q_dot(i),q_2dot(i),sigma(i),A{i}(1:3,1:3),r{i},r_cm{i},kr(i));
       assignin('base',strcat('omega',int2str(i)),omega);
       assignin('base',strcat('omega_dot',int2str(i)),omega_dot);
       assignin('base',strcat('acc_cm',int2str(i)),acc_cm);
       assignin('base',strcat('omega_dot_m',int2str(i)),omega_dot_m);
    end
    R=eye(3);
    Qdot=0;
    Q2dot=0;
    Im=zeros(3,3);
    redct=0;
    for i=length(sigma):-1:1
        omega=evalin('base',strcat('omega',int2str(i)));
        omega_dot=evalin('base',strcat('omega_dot',int2str(i)));
        acc_cm=evalin('base',strcat('acc_cm',int2str(i)));
        omega_dot_m=evalin('base',strcat('omega_dot_m',int2str(i)));
        [force,torque]=backward(force,torque,acc_cm,omega,omega_dot,Qdot,Q2dot,inertia{i},Im,R,r{i},r_cm{i},redct,m(i),R_m{i});
        Qdot=q_dot(i);
        Q2dot=q_2dot(i);
        Im=inertia_m(i);
        redct=kr(i);
        R=A{i}(1:3,1:3);
        out(i)=sigma(i)*force.'*R.'*[0 0 1].'+(1-sigma(i))*torque.'*R.'*[0 0 1].'+F_visc(i)*Qdot+F_stict(i)*sign(Qdot)+redct*Im*omega_dot_m.'*R_m{i}*[0 0 1].';
    end
    u=out;
end
%%add constant rotational matrices from link i to motor i+1