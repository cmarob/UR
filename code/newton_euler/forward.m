function [omegaN,omega_dotN,accN,accN_cm,omega_dotN_m] = forward(omega,omega_dot,acc,q_dot,q_2dot,sigma,R,r,r_cm,reduct)
%   forward recursion
    omegaN=R.'*(omega+(1-sigma)*q_dot*[0 0 1].');
    omega_dotN=R.'*(omega_dot+(1-sigma)*(q_2dot*[0 0 1].'+cross(q_dot*omega,[0 0 1].')));
    accN=R.'*(acc+sigma*q_2dot*[0 0 1].')+sigma*cross(2*q_dot*omegaN,R.'*[0 0 1].')+cross(omega_dotN,r)+cross(omegaN,cross(omegaN,r));
    accN_cm=accN+cross(omega_dotN,r_cm)+cross(omegaN,cross(omegaN,r_cm));
    omega_dotN_m=omega_dot+reduct*q_2dot*[0 0 1].'+cross(reduct*q_dot*omega,[0 0 1].');
end