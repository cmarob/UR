clear all;
close all;
clc;
%% dynamic quantities + information for building up the model

sigma=[0 0]; % defines the type(1=prismatic, 0=revolute) of motor and the quantity.

r_cm=cell(1,length(sigma)); % structure for CoMs vectors.
r_cm{1}=[.5 0 0].';         % CoMs have to be given in advance.
r_cm{2}=[.5 0 0].';

r_e=[.05 .05]; % defines the external radius of each link.
r_i=[0 0]; % defines the inner radius of each link if cave.
m=[2 1]; % links masses.

inertia=cell(1,length(sigma));          % structure for links' inertias
% inertia{1}=[.5 0 0;0 1 0;0 0 1];      % only cylindrical links are tought.
% inertia{2}=[.35 0 0;0 .75 0;0 0 .75]; % If inertias are provided is possible
                                        % to fill them manually, otherwise they
                                        % will be computed in NE(*,*,*)
                                        % function. Control is performed on the
                                        % norm, so leaving the matrices filled
                                        % with zeros or don't assing them
                                        % provide the same result.
F_visc=[.1 .1]; % viscous friction.
F_stict=[0 0]; % stiction friction.

DH=cell(1,length(sigma));   % Denavit-Hartenberg table.
DH{1}=[0 1 0 96069];        % Create the right number of cells and fill them 
DH{2}=[0 1 0 96069];        % with the appropriate numerical quantities, only
                            % the variables must be set to 96069.
                            % This trick is adopted to build up the DH
                            % matrices using numerical values.
                          
% leaving these last quantites to zero implies to neglect motors in calcula.
l_m=[0 0]; % motors' length.
r_e_m=[0 0]; % motors' external radius.
r_i_m=[0 0]; % motors' inner radius.
m_m=[0 0 0]; % motors' masses. Always one more wrt # of motors, the last is
             % a ficticious one used for inertia calcula, keep it equal to zero!
kr=[0 0]; % reduction ratios.
inertia_m=zeros(1,length(sigma));    % structure for motors' inertias.
% inertia_m(1)=.5                    % Same as for links' inertias but with
% inertia_m(2)=.5;                   % only the component for the rotating
                                     % axis.
                                     
R_m=cell(1,length(sigma));           % rotational matrix from RF i to motor's RF i+1
R_m{1}=diag([1 1 1]);                % to assign manually
R_m{2}=diag([1 1 1]);
%% initialization quantities

g=[0 9.81 0].';
omegaBase=[0 0 0].';
omega_dotBase=[0 0 0].';
accBase=[0 0 0].'-g;
forceEE=[0 0 0].';
torqueEE=[0 0 0].';

%% values of variables at given time

q=[pi/2 0].';
q_dot=[1 .25].';
q_2dot=[.05 .035].';

%% Newton-Euler routine

u=NE(q,q_dot,q_2dot)