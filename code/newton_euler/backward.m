function [forceN,torqueN] = backward(force,torque,acc_cm,omega,omega_dot,q_dot,q_2dot,inertia,inertia_m,R,r,r_cm,reduct,mass,R_m)
%   bacward recursion
    forceN = R*force+mass*acc_cm;
    torqueN = cross(-forceN,r+r_cm)+R*torque+cross(R*force,r_cm)+inertia*omega_dot+cross(omega,inertia*omega)+reduct*q_2dot*inertia_m*R_m*[0 0 1].'+cross(reduct*q_dot*inertia_m*omega,R_m*[0 0 1].');
end